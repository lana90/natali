<table>
  <thead>
    <tr>
      <th>Название</th>
      <th>XPath</th>
      <th>Full Xpath</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Логотип “Сбер Страхование”</td>
      <td>//*[@id="header"]//div[contains(@class,'sber-logo')]
      </td>
      <td>/html/body/app-root/header/div/div
      </td>
    </tr>
    <tr>
      <td>Хедер “Защита квартиры и дома онлайн”</td>
      <td>//div[contains(@class, 'header__text')]
      </td>
      <td>/html/body/app-root/div/app-policy-form/app-policy-form-header/div/div
      </td>
    </tr>
    <tr>
      <td>Название страницы “1. Выбор полиса”</td>
      <td>//p[contains(text(),'1. Выбор полиса')]</td>
      <td>
        /html/body/app-root/div/app-policy-form/div/app-policy-form-stepper/div[1]/div[1]/p</td>
    </tr>
    <tr>
      <td>
        Название страницы “2. Оформление”</td>
      <td>
        //p[contains(text(),'2. Оформление')]</td>
      <td>
        /html/body/app-root/div/app-policy-form/div/app-policy-form-stepper/div[1]/div[2]/p</td>
    </tr>
    <tr>
      <td>Название страницы “3. Подтверждение”</td>
      <td>
        //p[contains(text(),'3. Подтверждение')]</td>
      <td>
        /html/body/app-root/div/app-policy-form/div/app-policy-form-stepper/div[1]/div[3]/p</td>
    </tr>
    <tr>
      <td>Название страницы “4. Оплата”</td>
      <td>
        //p[contains(text(),'4. Оплата')]</td>
      <td>
        /html/body/app-root/div/app-policy-form/div/app-policy-form-stepper/div[1]/div[4]/p
    </tr>
    <tr>
      <td>Хедер “Что будет застраховано?”</td>
      <td>
        //h4[1][@class="block-header"="Что будет застраховано"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[1]
      </td>
    </tr>
    <tr>
      <td>Кнопка “Квартира” </td>
      <td>
        //*[@id="mat-button-toggle-1-button"]//span
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[1]/div/div[1]/mat-button-toggle/button/span
      </td>
    </tr>
    <tr>
      <td>Кнопка “Дом”</td>
      <td>
        //*[@id="mat-button-toggle-2-button"]//span
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[1]/div/div[2]/mat-button-toggle/button/span
      </td>
    </tr>
    <tr>
      <td>Хедер “Где находится недвижимость?”</td>
      <td>
        //h4[2][@class="block-header"="Где находится недвижимость?"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[2]
      </td>
    </tr>
    <tr>
      <td>Поле “Регион проживания”</td>
      <td>
        //input[@id="mat-input-0"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[1]/div/mat-form-field/div/div[1]/div[3]/input
      </td>
    </tr>
    <tr>
      <td>Надпись под полем “Регион проживания” - “Поле обязательное”</td>
      <td>
        //mat-error[@id="mat-error-0"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[1]/div/mat-form-field/div/div[2]/div/mat-error
      </td>
    </tr>
    <tr>
      <td>Хедер “Страховая защита включенная в программу”</td>
      <td>
        //h4[3][@class="block-header"="Страховая защита включенная в программу"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[3]
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Чрезвычайная ситуация”</td>
      <td>
        //div[contains(text(),'Чрезвычайная ситуация')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[1]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Механическое воздействие”</td>
      <td>
        //div[contains(text(),'Механическое воздействие')]</td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[2]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Удар молнии”</td>
      <td>
        //div[contains(text(),'Удар молнии')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[3]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Взрыв”</td>
      <td>
        //div[contains(text(),'Взрыв')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[4]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Залив”</td>
      <td>
        //div[contains(text(),'Залив')]
      </td>
      <td>
       /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[5]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Стихийные бедствия”</td>
      <td>
        //div[contains(text(),'Стихийные бедствия')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[1]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Противоправные действия третьих лиц”</td>
      <td>
        //div[contains(text(),'Противоправные действия третьих лиц')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[2]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Падение посторонних предметов”</td>
      <td>
        //div[contains(text(),'Падение посторонних предметов')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[3]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Падение летательных аппаратов и их частей”</td>
      <td>
        //div[contains(text(),'Падение летательных аппаратов и их частей')]
      </td>
      <td>
       /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[4]/div/div
      </td>
    </tr>
    <tr>
      <td>Текстовый блок “Пожар”</td>
      <td>
        //div[contains(text(),'Пожар')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[5]/div/div
      </td>
    </tr>
    <tr>
      <td>Хедер “Срок действия страхования”</td>
      <td>
        //h4[4][@class="block-header"="Срок действия страхования"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[4]
      </td>
    </tr>
    <tr>
    <td>Датапикер “Дата начала”</td>
      <td>
        //input[contains(@id,'mat-input-1')]
      </td>
      <td>
       /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[3]/div/mat-form-field/div/div[1]/div[3]/input
      </td>
    </tr>
    <tr>
    <td>Кнопка датапикера “Дата начала”</td>
      <td>
        //mat-datepicker-toggle//span[contains(@class,'mat-button-wrapper')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[3]/div/mat-form-field/div/div[1]/div[4]/mat-datepicker-toggle/button/span[1]
      </td>
    </tr>
    <tr>
      <td>Хедер “Особенности объекта”</td>
      <td>
        //h4[5][@class="block-header"="Особенности объекта"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[5]
      </td>
    </tr>
    <tr><td>Текстовый блок “Сдается в аренду”</td>
      <td>
        //mat-button-toggle-group[2]//*[contains(@class,'button-group-title')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[2]/div[1]
      </td>
    </tr>
    <tr>
      <td>Кнопка “Сдается в аренду” - “ДА”</td>
      <td>
        //*[contains(@id,'mat-button-toggle-22-button')]//span
      </td>
      <td>/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[2]/div[2]/div[1]/mat-button-toggle/button/span</td>
    </tr>
    <tr>
    <td>Кнопка “Сдается в аренду” - “Нет”</td>
      <td>
        //*[contains(@id,'mat-button-toggle-23-button')]//span
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[2]/div[2]/div[2]/mat-button-toggle/button/span
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Расположена на первом или последнем этаже”</td>
      <td>
        //mat-button-toggle-group[3]//*[contains(@class,'button-group-title')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[3]/div[1]
      </td>
    </tr>
    <tr>
    <td>Кнопка “Расположена на первом или последнем этаже” - “ДА”</td>
      <td>
        //*[contains(@id,'mat-button-toggle-25-button')]//span
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[3]/div[2]/div[1]/mat-button-toggle/button/span
      </td>
    </tr>
    <tr>
    <td>Кнопка “Расположена на первом или последнем этаже” - “Нет”</td>
      <td>
        //*[@id="mat-button-toggle-26-button"]/span
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[3]/div[2]/div[2]/mat-button-toggle/button/span
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Установлена охранная сигнализация”</td>
      <td>
        //mat-button-toggle-group[4]//*[contains(@class,'button-group-title')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[4]/div[1]
      </td>
    </tr>
    <tr>
    <td>Кнопка “Установлена охранная сигнализация” - “ДА”</td>
      <td>
        //*[contains(@id,'mat-button-toggle-28-button')]//span
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[4]/div[2]/div[1]/mat-button-toggle/button/span
      </td>
    </tr>
    <tr>
    <td>Кнопка “Установлена охранная сигнализация” - “Нет”</td>
      <td>
        //*[contains(@id,'mat-button-toggle-29-button')]//span
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[4]/div[2]/div[2]/mat-button-toggle/button/span
      </td>
    </tr>
    <tr>
    <td>Хедер “Страховая сумма и объекты страхования”</td>
      <td>
        //h4[6][@class="block-header ng-star-inserted"="Страховая сумма и объекты страхования"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[6]
      </td>
    </tr>
    <tr>
    <td>Надпись “Сумма” в блоке выбора суммы</td>
      <td>
        //span[@class="placeholder"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[1]/div/span[1]
      </td>
    </tr>
    <tr>
    <td>Надпись “цифры в ₽” в блоке выбора суммы </td>
      <td>
        //div[4]//input[@type="text"]
      </td>
      <td>
       /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[1]/div/input
      </td>
    </tr>
    <tr>
    <td>Значок “₽” в блоке выбора суммы </td>
      <td>
        //div[@class="input-block"]//span[2]
      </td>
      <td>
      /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[1]/div/span[2]
      </td>
    </tr>
    <tr>
    <td>Слайдер в блоке выбора суммы</td>
      <td>
        //div[@class="mat-slider-thumb"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[1]/mat-slider/div/div[3]/div[2]
      </td>
    </tr>
    <tr>
    <td>Надпись минимального значения под блоком выбора суммы - “900 000 ₽”</td>
      <td>
        //span[1][@class="ng-star-inserted"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[2]/span[1]
      </td>
    </tr>
    <tr>
    <td>Надпись максимального значения под  блоком выбора суммы - “6 000 000 ₽”</td>
      <td>
        //span[2][@class="ng-star-inserted"]
      </td>
      <td>
       /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[2]/span[2]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Стены и перекрытия”</td>
      <td>
        //div[contains(text(),'Стены и перекрытия')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[5]/div[1]
      </td>
    </tr>
    <tr>
    <td>Текстовый (цифровой) блок - “300 000 ₽”</td>
      <td>
       //*[contains(concat(' ', @class, ' '), ' policy-form ')]//*[contains(text(), 'Стены и перекрытия')]/following-sibling::*
      </td>
      <td>
       /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[5]/div[2]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Мебель, техника и ваши вещи”</td>
      <td>
       //div[contains(text(),'Мебель, техника и ваши вещи')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[6]/div[1]
      </td>
    </tr>
    <tr>
    <td>Текстовый (цифровой) блок - “100 000 ₽”</td>
      <td>
        //*[contains(concat(' ', @class, ' '), ' policy-form ')]//*[contains(text(), 'Мебель, техника и ваши вещи')]/following-sibling::*
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[6]/div[2]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Ремонт, электрика, сантехника, трубы, окна, двери”</td>
      <td>
       //div[contains(text(),'Ремонт, электрика, сантехника, трубы, окна, двери')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[7]/div[1]
      </td>
    </tr>
    <tr>
    <td>Текстовый (цифровой) блок - “300 000 ₽”</td>
      <td>
       //*[contains(concat(' ', @class, ' '), ' policy-form ')]//*[contains(text(), 'Ремонт, электрика, сантехника, трубы, окна, двери')]/following-sibling::*
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[7]/div[2]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Ответственность перед соседями”</td>
      <td>
       //div[contains(text(),'Ответственность перед соседями')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[8]/div[1]
      </td>
    </tr>
    <tr>
    <td>Текстовый (цифровой) блок - “200 000 ₽”</td>
      <td>
        //*[contains(concat(' ', @class, ' '), ' policy-form ')]//*[contains(text(), 'Ответственность перед соседями')]/following-sibling::*
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[8]/div[2]
      </td>
    </tr>
    <tr>
    <td>Хедер “Промокод”</td>
      <td>
        //h4[7][@class="block-header"="Промокод"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[7]
      </td>
    </tr>
    <tr>
    <td>Поле “Промокод”</td>
      <td>
        //div[@class="mat-form-field-infix ng-tns-c61-2"]//input[@type="text"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[9]/div[1]/mat-form-field/div/div[1]/div[3]/input
      </td>
    </tr>
    <tr>
    <td>Кнопка “Применить”</td>
      <td>
        //div[@class="reg-button"]//span[1][@class="mat-button-wrapper"="Применить"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[9]/div[2]/div/button/span[1]
      </td>
    </tr>
    <tr>
    <td>Хедер “Стоимость и срок действия”</td>
      <td>
        //h4[8][@class="block-header"="Стоимость и срок действия"]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[8]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Срок действия”</td>
      <td>
        //div[contains(text(),'Срок действия')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[10]/div[1]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “12 месяцев”</td>
      <td>
       //div[contains(text(),'12 месяцев')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[10]/div[2]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “Итоговая стоимость”</td>
      <td>
        //div[contains(text(),'Итоговая стоимость')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[11]/div[1]
      </td>
    </tr>
    <tr>
    <td>Текстовый блок “0,00 ₽”</td>
      <td>
       //div[contains(text(),'0,00 ₽')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[11]/div[2]
      </td>
    </tr>
    <tr>
    <td>Кнопка “Оформить”</td>
      <td>
        //div[contains(@class,'form-actions')]//span[contains(text(),'Оформить')]
      </td>
      <td>
        /html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[2]/button/span[1]
      </td>
    </tr>
    <tr>
      <td>Футер</td>
      <td>
        //*[contains(@class,'footer__content')]
      </td>
      <td>/html/body/app-root/footer/div</td>
    </tr>
  </tbody>
</table>